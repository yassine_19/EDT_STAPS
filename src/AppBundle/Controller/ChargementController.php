<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Enseignement;
use AppBundle\Entity\Element;
use AppBundle\Entity\Etudiant;
use AppBundle\Entity\Groupe;
use AppBundle\Entity\Creneau;
use AppBundle\Entity\Choix;
use AppBundle\Entity\Fixe;

use AppBundle\Entity\Maquette;

class ChargementController extends Controller
{
	/**
     * @Route("/chargement", name="chargement")
     */
    public function indexAction(Request $request)
    {
        return $this->render('chargement.html');
    }
    
    private function viderTables()
    {
    	//Récupération du manager doctrine de la base de données
    	$em = $this->getDoctrine()->getManager();
    	
    	$connexion=$em->getConnection();
		$platforme = $connexion->getDatabasePlatform();
		
		//Vider les tables, en rénitialisant la séquence des ids
		$connexion->exec('SET FOREIGN_KEY_CHECKS=0');
		$connexion->executeUpdate($platforme->getTruncateTableSQL('Maq_EC',true));
		$connexion->executeUpdate($platforme->getTruncateTableSQL('Maq_EC_Parcours',true));
		$connexion->executeUpdate($platforme->getTruncateTableSQL('Maq_UE',true));
		$connexion->executeUpdate($platforme->getTruncateTableSQL('Maq_Parcours',true));
		$connexion->executeUpdate($platforme->getTruncateTableSQL('Maq_Annee',true));
		$connexion->executeUpdate($platforme->getTruncateTableSQL('Maq_Annee_UE',true));
		$connexion->executeUpdate($platforme->getTruncateTableSQL('Maq_Choix',true));
		$connexion->executeUpdate($platforme->getTruncateTableSQL('Maq_Choix_EC',true));
		$connexion->exec('SET FOREIGN_KEY_CHECKS=1');
    }
    
    private function chargerAPS($excel_objet)
    {
    	//Récupération du manager doctrine de la base de données
    	$em = $this->getDoctrine()->getManager();
    	
    	//Récupération de la première feuille du fichier Excel
    	$feuille = $excel_objet->getSheet(0);
    	
    	//Récupération du nombre de lignes de la feuille
    	$derniere_ligne = $feuille->getHighestRow();
    	
    	//Parcours des lignes de la feuilles
    	for ($ligne = 2; $ligne <= $derniere_ligne; $ligne++)
    	{
    		//Création d'une nouvelle EC correspondante à l'APS
    		$ec=new Maquette\EC();
    		$ec->setId((int)$feuille->getCell('A'.$ligne)->getValue());
    		$ec->setIntitule($feuille->getCell('B'.$ligne)->getValue());
    		$ec->setCours($feuille->getCell('C'.$ligne)->getValue()=='oui'?true:false);
    		$ec->setTd($feuille->getCell('D'.$ligne)->getValue()=='oui'?true:false);
    		$ec->setTp($feuille->getCell('E'.$ligne)->getValue()=='oui'?true:false);
    		
    		//Enregistrement de l'EC dans la base	
    		$em->persist($ec);
    		$em->flush();
		}
    }
    
    private function chargerUEs($excel_objet)
    {
    	//Récupération du manager doctrine de la base de données
    	$em = $this->getDoctrine()->getManager();
    	
    	//Récupération du dépôt des parcours
    	$depot_parcours = $this->getDoctrine()->getRepository('AppBundle:Maquette\Parcours');
    	
    	//Création de la requette qui récupère une année à partir d'un parcours et d'un numéro
    	$requete_annee = $em->createQuery('SELECT a FROM AppBundle:Maquette\Annee a WHERE a.parcours=:parcours AND a.numero=:numero');
    	
    	//Récupération du dépôt des ECs
    	$depot_ECs = $this->getDoctrine()->getRepository('AppBundle:Maquette\EC');
    	
    	//Parcours des trois feuilles L1, L2 et L3
    	for($num_annee=1;$num_annee<=3;$num_annee++)
		{
			//Récupération de la feuille correspondante à l'année courante
			$feuille = $excel_objet->getSheet($num_annee);
    	
    		//Récupération de nombre de lignes de la feuille
			$derniere_ligne = $feuille->getHighestRow();
			for ($ligne = 2; $ligne <= $derniere_ligne; $ligne++)
			{	
				//Récupération du numéro de l'UE
				$num_ue=$feuille->getCell('A'.$ligne)->getValue();
				
				//Création d'une nouvelle UE				
				$ue=new Maquette\UE();
				$ue->setId($num_ue);
				$ue->setIntitule($feuille->getCell('B'.$ligne)->getValue());
				$ue->setLibre(false);
				
				$em->persist($ue);
				$em->flush();
				
				//Récupération des numéros de parcours contenant l'UE
				$nums_parcours=explode('-',$feuille->getCell('C'.$ligne));
				
				//Parcours des numéros de parcours
				for($i=0;$i<count($nums_parcours);$i++)
				{
					//Récupération du parcours
					$parcours = $depot_parcours->find($nums_parcours[$i]);
					
					//Si le parcours n'existe pas
					if($parcours==null)
					{
						//Création du parcours
						$parcours=new Maquette\Parcours();
						$parcours->setId((int)$nums_parcours[$i]);
						
						$em->persist($parcours);
						$em->flush();
					}
					
					//Récupération de l'année du parcours correspondante à l'année courante
					$requete_annee->setParameter('parcours',$parcours)->setParameter('numero',$num_annee);
					$annee = $requete_annee->setMaxResults(1)->getOneOrNullResult();
					
					//Si l'année n'existe pas
					if($annee==null)
					{	
						//Création de l'année
						$annee=new Maquette\Annee();
						$annee->setNumero($num_annee);
						$annee->setParcours($parcours);
				
						$em->persist($annee);
						$em->flush();
					}
					
					//Ajout de l'UE à l'année
					$annee->addUe($ue);
					$em->flush();
				}
				
			
				//Lecture et chargement des ECs de l'UE
				do
				{	
					//S'il ne s'agit pas d'un choix
					if($feuille->getCell('D'.$ligne)=='')
					{	
						//Création d'une EC
						$ec=new Maquette\EC();
						$ec->setId((int)$feuille->getCell('E'.$ligne)->getValue());
						$ec->setIntitule($feuille->getCell('F'.$ligne)->getValue());
						$ec->setCours($feuille->getCell('G'.$ligne)->getValue()=='oui'?true:false);
						$ec->setTd($feuille->getCell('H'.$ligne)->getValue()=='oui'?true:false);
						$ec->setTp($feuille->getCell('I'.$ligne)->getValue()=='oui'?true:false);
						
						$ec->setUe($ue);
						
						//Si l'EC est rattachée à des parcours spécifiques
						if($feuille->getCell('J'.$ligne)!="")
						{
							//Récupération des numéros de parcours spécifiques à l'EC
							$nums_parcours=explode('-',$feuille->getCell('J'.$ligne));
							
							//Parcours des numéros de parcours
							for($i=0;$i<count($nums_parcours);$i++)
							{
								//Récupération du parcours
								$parcours=$depot_parcours->find((int)$nums_parcours[$i]);
								
								//Si le parcours existe
								if($parcours!=null)
									$ec->addParcours($parcours);
								else//Sinon, on lance une exception
									throw new \Exception("Erreur à la ligne ".$ligne.": le numéro d'un parcours spécifique doit appartenir à la liste des parcours de l'UE");
							}
						}
				
						$em->persist($ec);
						$em->flush();
					}else//S'il s'agit d'un choix
					{
						//Création un choix
						$choix=new Maquette\Choix();
						$choix->setIntitule($feuille->getCell('F'.$ligne)->getValue());
						$choix->setUe($ue);
						
						//Récupération des numéros d'APS (ECs)
						$nums_aps=explode('-',$feuille->getCell('D'.$ligne));
						
						//Parcours des numéros d'APS
						for($i=0;$i<count($nums_aps);$i++)
						{
							//Récupération de l'APS
							$aps = $depot_ECs->find((int)$nums_aps[$i]);
							
							//Si l'APS existe
							if($aps!=null)
								$choix->addEC($aps);
							else//Sinon, on lance une exception
								throw new \Exception("Erreur à la ligne ".$ligne.": un numéro d'APS n'existe pas");
						}
						
						$em->persist($choix);
						$em->flush();
							
					}
					
					//On passe à la ligne suivante
					$ligne++;
					
				//Tant qu'on n'a pas dépassé la dernière ligne et (le numéro d'UE est vide ou le numéro d'UE n'a pas changé)
				}while($ligne<=$derniere_ligne && ($feuille->getCell('A'.$ligne)->getValue()=='' || $feuille->getCell('A'.$ligne)->getValue()==$num_ue));
				
				//On décrémente la ligne, puisque la boucle for l'incrémentera
				$ligne--;
			}
		}
   	}
   	
   	private function mettreAJourEnseignement()
   	{
   		//Récupération du manager doctrine de la base de données
   		$em = $this->getDoctrine()->getManager();
   		
   		//Récupération du dépôt des enseignements
   		$depot = $this->getDoctrine()->getRepository('AppBundle:Enseignement');
   		
   		//Création de la requete qui permet de récupérer un élément d'un enseignement à partir de son nom
   		$requete=$em->createQuery('SELECT e FROM AppBundle:Element e WHERE e.enseignement=:enseignement AND e.nom=:nom');
   		
   		//Création de la requête qui permet de supprimer un élément d'un enseignement à partir de son nom
   		$requete_suppression = $em->createQuery('DELETE FROM AppBundle:Element e WHERE e.enseignement = :enseignement AND e.nom=:nom');
   		
   		//Récupération des ecs
   		$ecs=$em->createQuery('SELECT p FROM AppBundle:Maquette\EC p')->getResult();
   		
   		//Parcours de la liste des ecs
   		foreach ($ecs as $ec)
   		{
   			//Tentative de récupération de l'enseignement correspondant à l'EC courant (qui a le même id)
   			$enseignement=$depot->find($ec->getId());
   			
   			//Si l'enseignement n'existe pas
   			if($enseignement==null)
   			{
   				//Création de l'enseignement
   				$enseignement=new Enseignement();
				$enseignement->setId($ec->getId());
				$enseignement->setPriorite(0);
			
				$em->persist($enseignement);
   			}
   			//Mise à jour du nom de l'enseignement
   			$enseignement->setNom($ec->getIntitule());
   			
   			$em->flush();
   			
   			//Récupération des éléments de l'enseignement
   			$cours=$requete->setParameter('enseignement',$enseignement)->setParameter('nom','Cours')->setMaxResults(1)->getOneOrNullResult();
   			$tp=$requete->setParameter('enseignement',$enseignement)->setParameter('nom','TP')->setMaxResults(1)->getOneOrNullResult();
   			$td=$requete->setParameter('enseignement',$enseignement)->setParameter('nom','TD')->setMaxResults(1)->getOneOrNullResult();
   			
   			//Mise à jour du cours
   			if($ec->getCours()==true)//Si l'EC a un cours
   			{
   				if($cours==null)//Si l'enseignement n'a pas de cours
   				{
   					//Création de l'élément Cours
	   				$element=new Element();
					$element->setNom('Cours');
					$element->setEnseignement($enseignement);
			
					$em->persist($element);
					$em->flush();
				}
   			}else//Si l'EC n'a pas de cours
   			{
   				if($cours!=null)//Si l'enseignement a un cours
   				{
   					//Suppression de l'élément
   					$requete_suppression->setParameter('enseignement',$enseignement)->setParameter('nom','Cours')->execute();
   				}
   			}
   			
   			//Mise à jour du tp
   			if($ec->getTp()==true)//Si l'EC a un tp
   			{
   				if($tp==null)//Si l'enseignement n'a pas de tp
   				{
   					//Création de l'élément Tp
	   				$element=new Element();
					$element->setNom('TP');
					$element->setEnseignement($enseignement);
			
					$em->persist($element);
					$em->flush();
				}
   			}else//Si l'EC n'a pas de tp
   			{
   				if($tp!=null)//Si l'enseignement a un tp
   				{
   					//Suppression de l'élément
   					$requete_suppression->setParameter('enseignement',$enseignement)->setParameter('nom','TP')->execute();
   				}
   			}
   			
   			//Mise à jour du td
   			if($ec->getTd()==true)//Si l'EC a un td
   			{
   				if($td==null)//Si l'enseignement n'a pas de td
   				{
   					//Création de l'élément Td
	   				$element=new Element();
					$element->setNom('TD');
					$element->setEnseignement($enseignement);
			
					$em->persist($element);
					$em->flush();
				}
   			}else//Si l'EC n'a pas de td
   			{
   				if($td!=null)//Si l'enseignement a un td
   				{
   					//Suppression de l'élément
   					$requete_suppression->setParameter('enseignement',$enseignement)->setParameter('nom','TD')->execute();
   				}
   			}
   		}
   		
   		//Suppression des enseignements dont les ids ne correspondent pas à des ECs chargées
   		$requete_suppression = $em->createQuery('DELETE FROM AppBundle:Enseignement e WHERE e.id NOT IN (SELECT p.id FROM AppBundle:Maquette\EC p)');
   		$requete_suppression->execute();
   	}
    
    /**
     * @Route("/chargement/charger_maquette", name="charger_maquette")
     */
    public function chargerMaquetteAction(Request $request)
    {	
    	try
    	{
    		//Si l'utilisateur n'a pas envoyé un fichier
    		if($request->files->get('fichier_maquette')==null)
    			throw new \Exception('Fichier non fourni');
    								
    		//Récupération du fichier envoyé
    		$fichier=$request->files->get('fichier_maquette');
    		
    		//Récupération de l'objet excel
    		$excel_objet = $this->get('phpexcel')->createPHPExcelObject($fichier);
    	
			//Vider les tables contenant les données de la maquette
			$this->viderTables();
    		
    		//Lecture et chargement des APS
    		$this->chargerAPS($excel_objet);
			
			//Lecture et Chargement des UEs
			$this->chargerUEs($excel_objet);
			
			//Mettre à jour les enseignements
			$this->mettreAJourEnseignement();
			
    	}catch(\Exception $e)
    	{
    		return $this->json(array('etat' => 'erreur','erreur'=>$e->getMessage()));
    	}
    	
    	
        return $this->json(array('etat' => 'chargee'));
    }
    
    /**
     * @Route("/chargement/charger_inscriptions", name="charger_inscriptions")
     */
    public function chargerInscriptionsAction(Request $request)
    {
        try
    	{
    		//Si l'utilisateur n'a pas envoyé un fichier
    		if($request->files->get('fichier_inscriptions')==null)
    			throw new \Exception('Fichier non fourni');
    			
    		//Récupération du fichier envoyé
    		$fichier=$request->files->get('fichier_inscriptions');
    		
    		//Récupération de l'objet excel
    		$excel_objet = $this->get('phpexcel')->createPHPExcelObject($fichier);		
    		
    		//Récupération du manager doctrine de la base de données
			$em = $this->getDoctrine()->getManager();
    	
    		//Récupération du dépôt des étudiants
    		$depot = $this->getDoctrine()->getRepository('AppBundle:Etudiant');
    		
    		//Création de la requête qui permet de récupérer tous les ids des enseignements que doit étudier l'étudiant obligatoirement, à partir des parcours et semestre auxquels il est inscrit
    		$requete_obligatoires=$em->createQuery('SELECT e.id FROM AppBundle:Maquette\Parcours p JOIN p.annees a JOIN a.ues u JOIN u.ecs e LEFT JOIN e.parcours ec_par WHERE p.id=:parcours_id AND a.numero=:numero_annee AND (ec_par.id is NULL OR ec_par.id=p.id)');
    		
    		//Création de la raquête qui permet de récupérer tous les ids des enseignements que doit rattraper l'étudiant à partir de la liste des UEs à rattraper et de le numéro de son parcours
    		$requete_rattraper=$em->createQuery('SELECT e.id FROM AppBundle:Maquette\UE u JOIN u.ecs e LEFT JOIN e.parcours ec_par WHERE u.id IN (:ids_ues) AND (ec_par.id is NULL OR ec_par.id=:parcours_id)');
    		    		
    		//Création de la requête permettant de récupérer tous les enseignements que doit étudier un étudiant à partir de la liste des ids des enseignements
    		$requete=$em->createQuery('SELECT ens FROM AppBundle:Enseignement ens WHERE ens.id in (:ids_enseignements)');
    		
    		//Création de la requête qui permet de récupérer tous les ids des choix d'un parcours et d'une année
    		$requete_choix=$em->createQuery('SELECT c.id FROM AppBundle:Maquette\Parcours p JOIN p.annees a JOIN a.ues u JOIN u.choix c WHERE p.id=:parcours_id AND a.numero=:numero_annee');
    		    		
    		//Création de la requête qui permet de récupérer tous les ids des choix à rattraper
    		$requete_choix_rattraper=$em->createQuery('SELECT c.id FROM AppBundle:Maquette\UE u JOIN u.choix c WHERE u.id IN (:ids_ues)');
    		
    		//Création de la requête qui permet de récupérer les enseignements d'un choix sauf ceux déjà affecté à un étudiant
			$requete_aps=$em->createQuery('SELECT ens FROM AppBundle:Enseignement ens WHERE ens.id IN (SELECT e.id FROM AppBundle:Maquette\Choix c JOIN c.ecs e WHERE c.id=:choix_id AND e.id NOT IN (SELECT h.id_enseignement FROM AppBundle:Sauvegarde\Choix_Historique h WHERE h.id_etudiant=:etudiant_id))');
			
			//Création de la requête qui permet de supprimer les choix d'un étudiant
   			$requete_suppression = $em->createQuery('DELETE FROM AppBundle:Choix c WHERE c.etudiant=:etudiant');
   		
			
    		//Récupération de la première feuille du fichier Excel
			$feuille = $excel_objet->getSheet(0);
			
			//Récupération du numéro de la dernière ligne du fichier Excel
			$derniere_ligne = $feuille->getHighestRow();
			
			$ids=array();//Tableau qui contiendra les ids des étudiants contenus dans le fichier Excel
			
			//Boucle de parcours des lignes de la feuille
			for ($ligne = 2; $ligne <= $derniere_ligne; $ligne++)
			{
				//Ajout de l'id de l'étudiant au tableau des ids
				$ids[]=(int)$feuille->getCell('A'.$ligne)->getValue();
				
				//Tentative de récupération de l'étudiant à partir de son id
				$etudiant=$depot->find((int)$feuille->getCell('A'.$ligne)->getValue());
				
				//Si l'étudiant correspondant à l'id n'existe pas dans la base de données
				if($etudiant==null)
				{
					//Création de l'étudiant
					$etudiant=new Etudiant();
					$etudiant->setId((int)$feuille->getCell('A'.$ligne)->getValue());
					$em->persist($etudiant);
				}
				
				//Mise à jour des nom et prénom de l'étudiant
				$etudiant->setNom($feuille->getCell('B'.$ligne)->getValue());
				$etudiant->setPrenom($feuille->getCell('C'.$ligne)->getValue());
				
				//Récupération des ids des enseignements obligatoires de l'étudiant
				$ids_obligatoires=$requete_obligatoires->setParameter('parcours_id',(int)$feuille->getCell('D'.$ligne)->getValue())
				->setParameter('numero_annee',(int)$feuille->getCell('E'.$ligne)->getValue())->getResult();
				
					
				//Récupération des ids des enseignements à rattraper de l'étudiant
				$ids_rattraper=$requete_rattraper->setParameter('ids_ues',array_map('intval',explode('-',$feuille->getCell('F'.$ligne))))
				->setParameter('parcours_id',(int)$feuille->getCell('D'.$ligne)->getValue())->getResult();
				
				//Récupération des enseignements obligatoires de l'étudiant
				$etudiant->setEnseignements($requete->setParameter('ids_enseignements',array_merge($ids_obligatoires,$ids_rattraper))->getResult());
				
				//Récupération des ids des choix possibles pour l'étudiant
				$choix_ids=$requete_choix->setParameter('parcours_id',(int)$feuille->getCell('D'.$ligne)->getValue())
				->setParameter('numero_annee',(int)$feuille->getCell('E'.$ligne)->getValue())->getResult();
				
				//Récupération des ids des choix à rattraper pour l'étudiant
				$choix_ids_rattraper=$requete_choix_rattraper->setParameter('ids_ues',array_map('intval',explode('-',$feuille->getCell('F'.$ligne))))->getResult();
				
				//Suppression des choix de l'étudiant
				$requete_suppression->setParameter('etudiant',$etudiant)->execute();
				
				//Concaténation des choix
				$choix_ids=array_unique(array_merge($choix_ids,$choix_ids_rattraper), $sort_flags = SORT_NUMERIC);
				
				//Parcours des ids des choix possibles pour l'étudiant
				foreach($choix_ids as $choix_id)
				{
					
					//Récupération des enseignements possibles pour le choix
					$enseignements=$requete_aps->setParameter('choix_id',$choix_id['id'])->setParameter('etudiant_id',$etudiant->getId())->getResult();
					
					//Création d'un nouveau choix
					$choix=new Choix();
					$choix->setEtudiant($etudiant);
					$choix->setEnseignements($enseignements);
					
					$em->persist($choix);
				}
				
				
				$em->flush();
				
			}
			
			//Suppression des étudiants dont les ids n'existent pas dans le fichier
			$requete_suppression = $em->createQuery('DELETE FROM AppBundle:Etudiant e WHERE e.id NOT IN (:ids)')->setParameter('ids',$ids);
   			$requete_suppression->execute();
			
    	}catch(\Exception $e)
    	{
    		return $this->json(array('etat' => 'erreur','erreur'=>$e->getMessage().' '.$e->getTraceAsString()));
    	}
    	
        return $this->json(array('etat' => 'chargee'));
    }
    
    //fonction qui permet de créer des créneaux à partir des parmètres
    private function creationCreneaux(\DateTime $date_debut,\DateTime $date_fin,$paire,$jour,$temps_debut,$temps_fin)
    {		
											
    	//La liste des créneaux
    	$creneaux=array();
    	
		//Récupération du pas: 1 ou 2 semaines
		if($paire)
			$pas=\DateInterval::createFromDateString('2 weeks');
		else
			$pas=\DateInterval::createFromDateString('1 week');
				
		//Récupération du numéro de jour de la semaine de la date début
		$jour_semaine = date('w',$date_debut->getTimestamp());
				
		//Cacul de différence entre le jour de la séance et le jour de la date début
		if($jour-$jour_semaine>=0)
		{
			$difference=new \DateInterval('P'.($jour-$jour_semaine).'D');
			$positive=true;
		}
		else
		{
			$difference=new \DateInterval('P'.($jour_semaine-$jour).'D');
			$positive=false;
		}
			
		//Calcul de la durée
		$duree=($temps_fin-$temps_debut)/60;
				
		//Boucle qui permet de parcourir les semaines à partir de la date début jusqu'à la date fin
		$date=new \Datetime();
		for($date->setTimestamp($date_debut->getTimestamp());$date->getTimestamp()<=$date_fin->getTimestamp();$date->add($pas))
		{
			//Création d'une nouvelle date qui sera stockée dans la base
			$d=new \DateTime();
				
			//Définition de la date
			$d->setTimestamp($date->getTimestamp());
			if($positive)
				$d->add($difference);
			else
				$d->sub($difference);
				
			//Définition de l'heure et les minutes
			$d->setTime((int)date('H',$temps_debut),(int)date('i',$temps_debut));
			
			//Création d'un nouveau créneau
			$creneau=new Creneau();
	
			$creneau->setDate($d);
			$creneau->setDuree($duree);
			
			$creneaux[]=$creneau;
		}
		return $creneaux;
    }
    
    private function chargerLibres($excel_objet)
    {
    	//Récupération du manager doctrine de la base de données
		$em = $this->getDoctrine()->getManager();
		
		//Récupération du dépôt correspondant aux étudiants
    	$depot_etudiants = $this->getDoctrine()->getRepository('AppBundle:Etudiant');
    	
    	//Création de la requête qui permet de supprimer touts les affectations fixes d'un étudiant
    	$requete_suppression = $em->createQuery('DELETE FROM AppBundle:Fixe f WHERE f.etudiant=:etudiant');
    	
    	//Récupération de la deuxième feuille Excel
		$feuille = $excel_objet->getSheet(1);
		
		//Récupération du numéro de la dernière ligne de la feuille
		$ligne = $feuille->getHighestRow();
		
		//Si le fichier contient une seule ligne
		if($ligne==2)
		{
			//Récupération de la date début et fin
			$date_debut=new \DateTime('@'.\PHPExcel_Shared_Date::ExcelToPHP($feuille->getCell('A'.$ligne)->getValue()));
			$date_fin=new \DateTime('@'.\PHPExcel_Shared_Date::ExcelToPHP($feuille->getCell('B'.$ligne)->getValue()));
			
			//Récupération de la périodicité de la séance
			if($feuille->getCell('C'.$ligne)->getValue()=='p')
				$paire=true;
			else
				$paire=false;
				
			//Récupération du numéro de jour de la semaine pendant lequel se passe la séance
			$jour=(int)$feuille->getCell('D'.$ligne)->getValue();
			
			//Récupération du temps début et fin
			$temps_debut=strtotime(\PHPExcel_Style_NumberFormat::toFormattedString($feuille->getCell('E'.$ligne)->getCalculatedValue(), 'hh:mm'));
			$temps_fin=strtotime(\PHPExcel_Style_NumberFormat::toFormattedString($feuille->getCell('F'.$ligne)->getCalculatedValue(), 'hh:mm'));
			
			//Récupération des étudiants
			$etudiants=$depot_etudiants->findAll();
			
			//Parcours de la liste des étudiants
			foreach($etudiants as $etudiant)
			{
				//suppression des affectations fixes d'un étudiant
				$requete_suppression->setParameter('etudiant',$etudiant);
				$requete_suppression->execute();
					
				//Création d'une affectation fixe
				$fixe=new Fixe();
				$fixe->setEtudiant($etudiant);
				$fixe->setLieu('');
				$fixe->setEnseignant('');
				
				$em->persist($fixe);
			
				//Création des créneaux fixes
				$creneaux=$this->creationCreneaux($date_debut,$date_fin,$paire,$jour,$temps_debut,$temps_fin);
				
				
				//Parcours des créneaux
				foreach($creneaux as $creneau)
				{
						
					$creneau->setFixe($fixe);
					$em->persist($creneau);
				}
				
				$em->flush();
			}
		}
		
    	
    }
    
    /**
     * @Route("/chargement/charger_groupes", name="charger_groupes")
     */
    public function chargerGroupesAction(Request $request)
    {
        try
    	{
    		//Si l'utilisateur n'a pas envoyé un fichier
    		if($request->files->get('fichier_groupes')==null)
    			throw new \Exception('Fichier non fourni');
    			
    		//Récupération du fichier envoyé
    		$fichier=$request->files->get('fichier_groupes');
    		
    		//Récupération de l'objet excel
    		$excel_objet = $this->get('phpexcel')->createPHPExcelObject($fichier);		
    		
    		//Récupération du manager doctrine de la base de données
			$em = $this->getDoctrine()->getManager();
    	
    		//Récupération du dépôt correspondant aux groupes
    		$depot_groupes = $this->getDoctrine()->getRepository('AppBundle:Groupe');
    		
    		//Création de la requête permettant de récupérer un élément à partir de l'identifiant de l'enseignement (=num de EC) et le type (cours/tp/td)
    		$requete=$em->createQuery('SELECT ele FROM AppBundle:Element ele JOIN ele.enseignement ens WHERE ens.id=:ens_id AND ele.nom=:type');
    		
    		//Création de la requête qui permet de supprimer les créneaux d'un groupe
    		$requete_suppression = $em->createQuery('DELETE FROM AppBundle:Creneau c WHERE c.groupe=:groupe');
				
    		//Récupération de la première feuille Excel
			$feuille = $excel_objet->getSheet(0);
			
			//Récupération du numéro de la dernière ligne de la feuille
			$derniere_ligne = $feuille->getHighestRow();
			
			$ids=array();//Tableau qui contiendra les ids des groupes contenus dans le fichier Excel
			
			//Boucle de parcours des lignes de la feuille
			for ($ligne = 2; $ligne <= $derniere_ligne; $ligne++)
			{
				//Ajout de l'id du groupe à la liste des ids des groupes traités
				$ids[]=(int)$feuille->getCell('A'.$ligne)->getValue();
				
				//Tentative de récupération du groupe à partir de son id
				$groupe=$depot_groupes->find((int)$feuille->getCell('A'.$ligne)->getValue());
				
				//Si le goupe correspondant à l'id n'existe pas dans la base de données
				if($groupe==null)
				{
					//Création d'un nouveau groupe
					$groupe=new Groupe();
					$groupe->setId((int)$feuille->getCell('A'.$ligne)->getValue());
					
					$em->persist($groupe);
				}else//Si le groupe existe déjà
				{
					//Suppression des créneaux du groupe
					$requete_suppression->setParameter('groupe',$groupe);
					$requete_suppression->execute();
				}
				
				//Mise à jour du lieu, enseignant et effectif max
				$groupe->setLieu($feuille->getCell('B'.$ligne)->getValue());
				$groupe->setEnseignant($feuille->getCell('C'.$ligne)->getValue());
				$groupe->setEffectifTotal($feuille->getCell('D'.$ligne)->getValue());
				
				//Récupération de l'élément
				$requete->setParameter('ens_id',(int)$feuille->getCell('E'.$ligne)->getValue())
				->setParameter('type',$feuille->getCell('F'.$ligne)->getValue());
				$element=$requete->setMaxResults(1)->getOneOrNullResult();
				
				if($element==null)//Si on ne parvient pas à récupérer l'élément => on retourne une erreur à l'utilisateur
					throw new \Exception('Erreur à la ligne'.$ligne.': numéro d\'EC introuvable ou l\'EC ne comporte pas l\'élément spécifié (cours/tp/td)');
				
				//Mise à jour de l'élément auquel est affecté le groupe
				$groupe->setElement($element);
				
				//Récupération de la date début et fin
				$date_debut=new \DateTime('@'.\PHPExcel_Shared_Date::ExcelToPHP($feuille->getCell('G'.$ligne)->getValue()));
				$date_fin=new \DateTime('@'.\PHPExcel_Shared_Date::ExcelToPHP($feuille->getCell('H'.$ligne)->getValue()));
				
				//Récupération de la périodicité de la séance
				if($feuille->getCell('I'.$ligne)->getValue()=='p')
					$paire=true;
				else
					$paire=false;
					
				//Récupération du numéro de jour de la semaine pendant lequel se passe la séance
				$jour=(int)$feuille->getCell('J'.$ligne)->getValue();
				
				//Récupération du temps début et fin
				$temps_debut=strtotime(\PHPExcel_Style_NumberFormat::toFormattedString($feuille->getCell('K'.$ligne)->getCalculatedValue(), 'hh:mm'));
				$temps_fin=strtotime(\PHPExcel_Style_NumberFormat::toFormattedString($feuille->getCell('L'.$ligne)->getCalculatedValue(), 'hh:mm'));
				
				//Création des créneaux
				$creneaux=$this->creationCreneaux($date_debut,$date_fin,$paire,$jour,$temps_debut,$temps_fin);

				//Parcours des créneaux
				foreach($creneaux as $creneau)
				{
					
					$creneau->setGroupe($groupe);
					$em->persist($creneau);
				}
								
				
				$em->flush();
			}
			
			//Suppression des groupes dont les ids n'existent pas dans le fichier
			$requete_suppression = $em->createQuery('DELETE FROM AppBundle:Groupe g WHERE g.id NOT IN (:ids)')->setParameter('ids',$ids);
   			$requete_suppression->execute();
   			
   			//Charger les créneaux fixes correspondants uax ues libres
   			$this->chargerLibres($excel_objet);
			
    	}catch(\Exception $e)
    	{
    		return $this->json(array('etat' => 'erreur','erreur'=>$e->getMessage()));
    	}
    	
        return $this->json(array('etat' => 'chargee'));
    }
}
