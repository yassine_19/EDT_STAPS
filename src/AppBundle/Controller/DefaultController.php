<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Sauvegarde;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
    	return $this->render('page.html');
    }
    
    
    private function creerPeriodes()
    {
    	//Récupération du manager doctrine de la base de données
    	$em = $this->getDoctrine()->getManager();
    	
    	//Récupération du dépôt des périodes
    	$depot = $this->getDoctrine()->getRepository('AppBundle:Sauvegarde\Periode');
    	
    	//Année actuelle
    	$annee=(int)date('y')+1;
    	
    	//Tentative de récupération de la période (annee-1,1)
    	$periode1=$depot->findOneBy(array('annee' => $annee-1,'periode' => 1));
    	
    	//Si la période n'existe pas
    	if($periode1==null)
    	{
    		$periode1=new Sauvegarde\Periode();
 			$periode1->setAnnee($annee-1);
 			$periode1->setPeriode(1);
 			
 			$em->persist($periode1);
 			$em->flush();
    	}
    	
    	//Tentative de récupération de la période (annee-1,2)
    	$periode2=$depot->findOneBy(array('annee' => $annee-1,'periode' =>2));
    	
    	//Si la période n'existe pas
    	if($periode2==null)
    	{
    		$periode2=new Sauvegarde\Periode();
 			$periode2->setAnnee($annee-1);
 			$periode2->setPeriode(2);
 			
 			$em->persist($periode2);
 			$em->flush();
    	}
    	
    	//Tentative de récupération de la période (annee,1)
    	$periode3=$depot->findOneBy(array('annee' => $annee,'periode' =>1));
    	
    	//Si la période n'existe pas
    	if($periode3==null)
    	{
    		$periode3=new Sauvegarde\Periode();
 			$periode3->setAnnee($annee);
 			$periode3->setPeriode(1);
 			
 			$em->persist($periode3);
 			$em->flush();
    	}
    	
    	//Tentative de récupération de la période (annee,2)
    	$periode4=$depot->findOneBy(array('annee' => $annee,'periode' =>2));
    	
    	//Si la période n'existe pas
    	if($periode4==null)
    	{
    		$periode4=new Sauvegarde\Periode();
 			$periode4->setAnnee($annee);
 			$periode4->setPeriode(2);
 			
 			$em->persist($periode4);
 			$em->flush();
    	}
    	
    	if((int)date('n')>=8)
    		return $periode3;
    	else
    		return $periode2;
    	
    }
    /**
     * @Route("/recuperer_periodes", name="recupere_periodes")
     */
    public function recupererPeriodesAction(Request $request)
    {	
    	//Création des périodes manquantes et récupération de la période la plus qui correspond le plus probablement à la période courante
    	$periode_courante=$this->creerPeriodes();
    	
    	//Récupération du manager doctrine de la base de données
    	$em = $this->getDoctrine()->getManager();
    	
    	//Récupération de la période actuelle
    	$periode_actuelle=$em->createQuery('SELECT p FROM AppBundle:Sauvegarde\Periode_Actuelle p')->setMaxResults(1)->getOneOrNullResult();
    	
    	//Si la période actuelle n'est pas définie
    	if($periode_actuelle==null)
    	{
    		$periode_actuelle=new Sauvegarde\Periode_Actuelle();
    		$periode_actuelle->setPeriode($periode_courante);
    		
    		$em->persist($periode_actuelle);
    		$em->flush();
    	}
    		
    	//Récupération des périodes
    	$periodes=$em->createQuery('SELECT p FROM AppBundle:Sauvegarde\Periode p ORDER BY p.annee,p.periode')->getResult();
    	
    	$liste=array();
    	$periode_index=0;
    	
    	//Conctruction de la liste des objets à retourner
   		foreach($periodes as $index=>$periode)
   		{
   			$liste[]=array('id'=>$periode->getId(),'annee'=>$periode->getAnnee(),'periode'=>$periode->getPeriode(),'chaine'=>$periode->getAnnee().'/'.($periode->getAnnee()+1).' S'.$periode->getPeriode());
   			
   			if($periode_actuelle->getPeriode()->getId()==$periode->getId())
   				$periode_index=$index;
   		}
    	
    	return $this->json(array('periodes' => $liste,'periode_index' => $periode_index));
    }
    
    /**
     * @Route("/changer_periode", name="changer_periode")
     */
    public function changerPeriodeAction(Request $request)
    {
    	try
    	{
		    //Vérification que l'utilisateur a envoyé un contenu non vide
		    if(!empty($request->getContent()))
		    {
		    	//Récupération de l'identifiant de la nouvelle période
		    	$contenu=json_decode($request->getContent(),true);
		    	$id_periode=(int)$contenu['id_periode'];
		    	
		    	//Récupération du manager doctrine de la base de données
	   			$em = $this->getDoctrine()->getManager();
		    	
		    	//Récupération du dépôt des périodes
				$depot = $this->getDoctrine()->getRepository('AppBundle:Sauvegarde\Periode');
			
		    	//Récupération de la période correspondante
		    	$periode = $depot->find($id_periode);
		    	
		    	//Modifier la période actuelle
		    	$em->createQuery('UPDATE AppBundle:Sauvegarde\Periode_Actuelle p SET p.periode=:periode')->setParameter('periode',$periode)->execute();
			}
		}catch(\Exception $e)
    	{
    		return $this->json(array('etat' => 'erreur','erreur'=>$e->getMessage()));
    	}    	
    	return $this->json(array('etat' => 'changee'));
    }
}

