<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use AppBundle\Entity\Enseignement;
use AppBundle\Entity\Affectation;

use AppBundle\Entity\Sauvegarde;

class EmploisTempsController extends Controller
{
	/**
     * @Route("/emploisTemps", name="emploisTemps")
     */
    public function indexAction(Request $request)
    {
        return $this->render('emploisTemps.html');
    }
    
    /**
     * @Route("/emploisTemps/recuperer_etudiants", name="recuperer_etudiants")
     */
    public function recupererEtudiantsAction(Request $request)
    {
    	//Récupération du manager doctrine de la base de données
   		$em = $this->getDoctrine()->getManager();
   		
   		//Création de la requette permettant de récupérer la liste des Etudiants par orde alphabétique
   		$requete = $em->createQuery('SELECT p FROM AppBundle:Etudiant p ORDER BY p.nom,p.prenom');
   		
   		//Récupération des étudiants
   		$etudiants=$requete->getResult();
   		
   		//La liste à retourner
   		$liste=array();
   		
   		//Conctruction de la liste des objets à retourner
   		foreach($etudiants as $etudiant)
   		{
   			$liste[]=array('id'=>$etudiant->getId(),'nom'=>$etudiant->getNom(),'prenom'=>$etudiant->getPrenom());
   		}
   		
        return $this->json(array('liste'=>$liste));
    }
    
    /**
     * @Route("/emploisTemps/recuperer_semaines", name="recuperer_semaines")
     */
    public function recupererSemainesAction(Request $request)
    {
    	//Récupération de la date actuelle
    	$date_actuelle=new \DateTime();
    	
    	//Récupération de l'année actuelle
    	$annee_actuelle=$date_actuelle->format('Y');
    	
   		//Récupération du numéro de jour de la semaine de la date actuelle
		$jour = date('w',$date_actuelle->getTimestamp());
		
		//Cacul de différence entre le jour de la date atuelle et le lundi de la même semaine (ou semaine prochaine si c'est dimanche)
		if($jour==0)
		{
			$difference=new \DateInterval('P1D');
			$lundi=$date_actuelle->add($difference);
		}
		else
		{
			$difference=new \DateInterval('P'.($jour-1).'D');
			$lundi=$date_actuelle->sub($difference);
		}
		
		
		//La liste à retourner
   		$liste=array();
   		
   		//Création du pas de parcours de semaines
   		$pas=\DateInterval::createFromDateString('1 week');
   		
   		//compteur
   		$compteur=0;
   		
		//Parcours des lundi précédents
		$date=new \Datetime();
		for($date->setTimestamp($lundi->getTimestamp());$date->format('Y')>=$annee_actuelle-1;$date->sub($pas))
		{
			$compteur++;
			array_unshift($liste,$date->format('d/m/Y'));
		}
		
		//Parcours des lundi suivants
		$date->setTimestamp($lundi->getTimestamp());
		for($date->add($pas);$date->format('Y')<=$annee_actuelle+1;$date->add($pas))
		{
			$liste[]=$date->format('d/m/Y');
		}	
 		  		
   		
        return $this->json(array('liste'=>$liste,'index_semaine_actuelle'=>$compteur-1));
    }
    
    /**
     * @Route("/emploisTemps/recuperer_emploi_temps", name="recuperer_emploi_temps")
     */
    public function recupererEmploiTempsAction(Request $request)
    {
    	//L'objet à retourner
	   	$emploi=[];
	   		
    	//Vérification que l'utilisateur a envoyé un contenu non vide
        if(!empty($request->getContent()))
        {
        	//Récupération de l'id de l'étudiant et la date de la semaine
        	$contenu=json_decode($request->getContent(),true);
        	$id_etudiant=(int)$contenu['id_etudiant'];
        	$semaine=$contenu['semaine'];
        	
        	//Création des dates de début et fin de la semaine
        	$lundi=\DateTime::createFromFormat('d/m/Y',$semaine);
        	$dimanche=\DateTime::createFromFormat('d/m/Y',$semaine);
        	$dimanche->add(new \DateInterval('P6D'));
        		
    		//Récupération du manager doctrine de la base de données
   			$em = $this->getDoctrine()->getManager();
   		
	   		//Création de la requette permettant de récupérer les créneaux non fixes d'un étudiant pendant une semaine
	   		$requete = $em->createQuery('SELECT CONCAT(ele.nom,\' \',ens.nom,\'--\',gr.enseignant,\'--\',gr.lieu) as evenement,cre.date as date,cre.duree as duree FROM AppBundle:Creneau cre JOIN cre.groupe gr JOIN gr.affectations aff JOIN aff.element ele JOIN ele.enseignement ens JOIN aff.etudiant etu WHERE etu.id=:id_etudiant AND cre.date BETWEEN :lundi AND :dimanche');
	   		
	   		//Récupération des créneaux
	   		$creneaux=$requete->setParameter('id_etudiant',$id_etudiant)->setParameter('lundi',$lundi)->setParameter('dimanche',$dimanche)->getResult();
	   		
	   		//Création de la requette permettant de récupérer les créneaux fixes d'un étudiant pendant une semaine
	   		$requete = $em->createQuery('SELECT CONCAT(\'Enseignement Extérieur\',\'--\',fix.enseignant,\'--\',fix.lieu) as evenement,cre.date as date,cre.duree as duree FROM AppBundle:Creneau cre JOIN cre.fixe fix JOIN fix.etudiant etu WHERE etu.id=:id_etudiant AND cre.date BETWEEN :lundi AND :dimanche');
	   		
	   		//Ajout des créneaux fixes:
	   		$creneaux=array_merge($creneaux,$requete->setParameter('id_etudiant',$id_etudiant)->setParameter('lundi',$lundi)->setParameter('dimanche',$dimanche)->getResult());
	   		
	   		
	   		//Parcours de la liste des créneaux
	   		foreach($creneaux as $creneau)
	   		{
	   			//Récupération du numéro de jour du créneau
	   			$jour=date('w',$creneau['date']->getTimestamp());
	   			
	   			//Récupération des données du créneauS
	   			$evenement=$creneau['evenement'];
	   			$heure_debut=(int)$creneau['date']->format('G');
	   			$min_debut=(int)$creneau['date']->format('i');
	   			$heure_fin=(int)$creneau['date']->add(new \DateInterval('PT'.$creneau['duree'].'M'))->format('G');
	   			$min_fin=(int)$creneau['date']->add(new \DateInterval('PT'.$creneau['duree'].'M'))->format('i');
	   			
	   			switch($jour)
				{
					case 1:
						$emploi['lundi'][]=array('evenement'=>$evenement,'heure_debut'=>$heure_debut,'min_debut'=>$min_debut,'heure_fin'=>$heure_fin,'min_fin'=>$min_fin);
					break;
					case 2:
						$emploi['mardi'][]=array('evenement'=>$evenement,'heure_debut'=>$heure_debut,'min_debut'=>$min_debut,'heure_fin'=>$heure_fin,'min_fin'=>$min_fin);
					break;
					case 3:
						$emploi['mercredi'][]=array('evenement'=>$evenement,'heure_debut'=>$heure_debut,'min_debut'=>$min_debut,'heure_fin'=>$heure_fin,'min_fin'=>$min_fin);
					break;
					case 4:
						$emploi['jeudi'][]=array('evenement'=>$evenement,'heure_debut'=>$heure_debut,'min_debut'=>$min_debut,'heure_fin'=>$heure_fin,'min_fin'=>$min_fin);
					break;
					case 5:
						$emploi['vendredi'][]=array('evenement'=>$evenement,'heure_debut'=>$heure_debut,'min_debut'=>$min_debut,'heure_fin'=>$heure_fin,'min_fin'=>$min_fin);
					break;
					case 6:
						$emploi['dimanche'][]=array('evenement'=>$evenement,'heure_debut'=>$heure_debut,'min_debut'=>$min_debut,'heure_fin'=>$heure_fin,'min_fin'=>$min_fin);
					break;
				}
	   		}
	   		
	   	}
	   	
	   	return $this->json($emploi);
    }
}
