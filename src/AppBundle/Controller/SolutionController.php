<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use AppBundle\Entity\Enseignement;
use AppBundle\Entity\Affectation;

use AppBundle\Entity\Sauvegarde;

class SolutionController extends Controller
{
	/**
     * @Route("/solution", name="solution")
     */
    public function indexAction(Request $request)
    {
        return $this->render('solution.html');
    }
    
    /**
     * @Route("/solution/recuperer_priorites", name="recuperer_priorites")
     */
    public function recupererPrioritesAction(Request $request)
    {
    	//Récupération du manager doctrine de la base de données
   		$em = $this->getDoctrine()->getManager();
   		
   		//Création de la requette permettant de récupérer la liste des Enseignements
   		$requete = $em->createQuery('SELECT p FROM AppBundle:Enseignement p');
   		
   		//Récupération des enseignements
   		$enseignements=$requete->getResult();
   		
   		$liste=array();
   		
   		//Conctruction de la liste des objets à retourner
   		foreach($enseignements as $enseignement)
   		{
   			$liste[]=array('id'=>$enseignement->getId(),'nom'=>$enseignement->getNom(),'priorite'=>$enseignement->getPriorite());
   		}
   		
        return $this->json(array('liste'=>$liste));
    }
    
    /**
     * @Route("/solution/sauvegarder_priorites", name="sauvegarder_priorites")
     */
    public function sauvegarderPrioritesAction(Request $request)
    {
        try
        {
        	//Vérification que l'utilisateur a envoyé un contenu non vide
        	if(!empty($request->getContent()))
        	{
        		//Récupération de la liste des enseignements
        		$contenu=json_decode($request->getContent(),true);
        		$liste=$contenu['liste'];
        		
        		//Récupération du manager doctrine de la base de données
   				$em = $this->getDoctrine()->getManager();
        		
        		//Création de la requete qui permet de modifier la priorité de chaque enseignement
        		$requete= $em->createQuery('UPDATE AppBundle:Enseignement e SET e.priorite=:priorite WHERE e.id=:id');
        		
        		foreach($liste as $donnee)
        		{
        			$requete->setParameter('id',$donnee['id'])->setParameter('priorite',$donnee['priorite'])->execute();
        		}
    		}	
        }catch(\Exception $e)
    	{
    		return $this->json(array('etat' => 'erreur','erreur'=>$e->getMessage()));
    	}
    	
        return $this->json(array('etat' => 'sauvegardee'));
    }
    
    /**
     * @Route("/solution/generer_solution", name="generer_solution")
     */
    public function genererSolutionAction(Request $request)
    {
        try
        {
        	//Récupération du manager doctrine de la base de données
   			$em = $this->getDoctrine()->getManager();
   			
   			//vider la table des affectations
   			$connexion=$em->getConnection();
			$platforme = $connexion->getDatabasePlatform();
			$connexion->executeUpdate($platforme->getTruncateTableSQL('Affectation',true));
   			
   			//Récupération des enseignements ordonnés selon la priorité
   			$requete= $em->createQuery('SELECT e FROM AppBundle:Enseignement e ORDER BY e.priorite DESC');
   			$enseignements=$requete->getResult();
   			
   			//*****Enseignements obligatoires
   			//Parcours de la liste des enseignements
   			foreach($enseignements as $enseignement)
   			{
   				//Récupération des étudiants qui doivent obligatoirment étudier cet enseignement
   				$etudiants=$enseignement->getEtudiants();
   				
   				//Récupération des éléments de l'enseignement
   				$elements=$enseignement->getElements();
   				
   				//Parcours de la liste des étudiants
   				foreach($etudiants as $etudiant)
   				{
   					//Parcours de la liste des éléments
   					foreach($elements as $element)
   					{
   						//Récupération des groupes disponibles pour l'élément
   						$groupes=$element->getGroupesDisponibles();
   						 			 
   						//Parcours des groupes
   						foreach($groupes as $groupe)
   						{
   						  
   							//Vérification que les créneaux des groupes sont campatibles avec les créneaux de l'étudiant
   							if($etudiant->verifierDisponibilite($groupe))
   							{
   								//Création d'un nouvelle affectation
   								$affectation=new Affectation();
   								
   								$affectation->setEtudiant($etudiant);
   								
   								//mise à jour des créneaux de l'étudiant
   								$etudiant->ajouterCreneaux($groupe);
   								
   								$affectation->setGroupe($groupe);
   								
   								//mise à jour de l'effectif actuel du groupe
   								$groupe->incrementerEffectif();
   								
   								$affectation->setElement($element);
   								
   								//mise à jour de la liste des groupes disponibles
   								$element->mettreAJourGroupesDisponibles($groupe);
   								
   								$em->persist($affectation);
   								$em->flush();
   								
   								//On passe à l'élément suivant
   								break;
   							}
   							
   						}
   					}
   				}
   			}
   			
   			
   			//*****Choix
   			//Parcours de la liste des enseignements
   			foreach($enseignements as $enseignement)
   			{
   				//Récupération des choix pointants sur l'enseigement
   				$choix=$enseignement->getChoix();
   				
   				//Récupération des éléments de l'enseignement
   				$elements=$enseignement->getElements();
   				
   				//Parcours de la liste des choix
   				foreach($choix as $choi)
   				{
   					//Si le choix est déjà affecté (satisfait)
   					if($choi->isAffecte())
   						continue; //On passe au choix suivant
   					
   					//Récupération de l'étudiant possédant le choix
   					$etudiant=$choi->getEtudiant();
   					
   					//Est-ce qu'il faut annuler
   					$annulation=false;
   					
   					//La liste des affectations à sauvegarder en cas de non annulation
   					$affectations_temp=array();
   					
   					//la liste des groupes à mettre à jour en cas de non annulation
   					$groupes_temp=array();
   					
   					//Parcours de la liste des éléments
   					foreach($elements as $element)
   					{
   						//Est-ce qu'un élément n'a pas pu être affecté
   						$impossible=true;
   					
   						//Récupération des groupes disponibles pour l'élément
   						$groupes=$element->getGroupesDisponibles();
   						 			 
   						//Parcours des groupes
   						foreach($groupes as $groupe)
   						{
   							//Vérification que les créneaux des groupes sont campatibles avec les créneaux de l'étudiant
   							if($etudiant->verifierDisponibilite($groupe))
   							{
   							
   					   								
   								//Création d'un nouvelle affectation
   								$affectation=new Affectation();
   								$affectation->setEtudiant($etudiant);
   								$affectation->setGroupe($groupe);
   								$affectation->setElement($element);
   								
   								$affectations_temp[]=$affectation;
   								
   								//mise à jour des créneaux de l'étudiant
   								$etudiant->ajouterCreneaux($groupe);
   								
   								$groupes_temp[]=$groupe;
   								   								   								
   								//L'élément a pu être affecté à un groupe
   								$impossible=false;
   								
   								//On passe à l'élément suivant
   								break;
   							}
   						}
   						
   						//Si l'élément n'a pas pu être affecté
   						if($impossible)
   						{
   							//Il faut annuler
   							$annulation=true;
   							
   							//Pas la peine de vérifier les autres éléments
   							break;
   						}
   					}
   					
   					//Si tous les éléments ont été affecté
   					if(!$annulation)
   					{
   						
   						//Parcours de la liste des groupes à mettre à jour
   						foreach($groupes_temp as $groupe)
   						{
   							//Incrémentation de l'effectif actuel du groupe
   							$groupe->incrementerEffectif();
   								
   							//mise à jour de la liste des groupes disponibles de l'élément du groupe
   							$groupe->getElement()->mettreAJourGroupesDisponibles($groupe);
   						}
   						
   						//Parcours de la liste des affectations à sauvegarder
   						foreach($affectations_temp as $affectation)
   						{
   							
   							$em->persist($affectation);
   							$em->flush();
   						}
   								
   						$choi->rendreAffecte();
   					}
   					else //Si un élément n'a pas pu être affecté
   					{
   						//Annulation des créneaux ajoutés à l'étudiant correspondants à la liste des groupes à mettre à jour en cas de non annulation
   						foreach($groupes_temp as $groupe)
   						{
   							$etudiant->enleverCreneaux($groupe);
   						}
   					}
   				}
   			}
    	}catch(\Exception $e)
    	{
    		return $this->json(array('etat' => 'erreur','erreur'=>$e->getMessage()));
    	}
    	
        return $this->json(array('etat' => 'generee'));
    }
    
    /**
     * @Route("/solution/recuperer_solution", name="recuperer_solution")
     */
    public function recupererSolutionAction(Request $request)
    {
    	
    	//Récupération du manager doctrine de la base de données
   		$em = $this->getDoctrine()->getManager();
   		
   		//Récupération de la liste des enseignements avec leurs éléments et les étudiants qui l'étudient
    	$requete=$em->createQuery('SELECT ens.nom as nom_ens,ele.id as id_ele,ele.nom as nom_ele,etu.id as id_etu,etu.nom as nom_etu,etu.prenom as prenom_etu FROM AppBundle:Etudiant etu JOIN etu.enseignements ens JOIN ens.elements ele ORDER BY ens.priorite DESC,ens.id,ele.nom');
    	$lignes=$requete->getResult();
    	
    	//Création de la requête qui pemet de retourner l'identifiant du groupe affecté à un étudiant pour un élément
    	$requete=$em->createQuery('SELECT gr.id FROM AppBundle:Affectation aff JOIN aff.groupe gr JOIN aff.etudiant etu JOIN aff.element ele WHERE etu.id=:id_etudiant AND ele.id=:id_element');
    	
    	$obligatoires=array();
   		
   		//Conctruction de la liste des objets à retourner
   		foreach($lignes as $ligne)
   		{
   			//Récupération de l'identifiant du groupe affecté à l'étudiant courant pour l'élément courant
   			$id_groupe=$requete->setParameter('id_etudiant',$ligne['id_etu'])->setParameter('id_element',$ligne['id_ele'])->setMaxResults(1)->getOneOrNullResult();
   			
   			//S'il n'y a pas de gourpe affecté
   			if($id_groupe==null)
   				$probleme=true;
   			else
   				$probleme=false;
   			
   			$obligatoires[]=array('enseignement'=>$ligne['nom_ens'],'element'=>$ligne['nom_ele'],'etudiant'=>$ligne['nom_etu'].' '.$ligne['prenom_etu'],'groupe'=>$id_groupe['id'],'probleme'=>$probleme);
   		}
   		
   		//Récupération des étudiants
   		$requete=$em->createQuery('SELECT etu FROM AppBundle:Etudiant etu ORDER BY etu.nom,etu.prenom');
   		$etudiants=$requete->getResult();
   		
   		//Création de la requête qui permet de récupérer l'affectation d'un étudiant pour un élément
   		$requete=$em->createQuery('SELECT aff FROM AppBundle:Affectation aff WHERE aff.etudiant=:etudiant AND aff.element=:element');
   		
   		$choix=array();
   		
   		//Parcours de la liste des étudiants
   		foreach($etudiants as $etudiant)
   		{
   			//Récupération des choix de l'étudiant
   			$choixs=$etudiant->getChoix();
   			
   			//Parcours de la liste des choix de l'étudiant
   			foreach($choixs as $choi)
   			{
   				
   				//Récupération des enseignements du choix
   				$enseignements=$choi->getEnseignements();
   				
   				//L'enseignement choisi
   				$enseignement_choisi=null;
   				
   				//Définition des choix
   				$chaine_choix='';
   				foreach($enseignements as $enseignement)
   				{
   					$chaine_choix=$chaine_choix.' || '.$enseignement->getNom();
   				}
   				
   				//Parcours de la liste des enseignements
   				foreach($enseignements as $enseignement)
   				{
   		
   					//Initialisation de la chaine représentant les groupes
   					$chaine_groupes='';
   					
   					//Est-ce qu'un élément n'est pas affecté
   					$probleme=false;
   				
   					//Récupération de la liste des éléments de l'enseignement
   					$elements=$enseignement->getElements();
   					
   					//Parcours de la liste des éléments
	   				foreach($elements as $element)
	   				{
	   					//Récupération de l'affectation de l'étudiant pour l'élément
	   					$affectation=$requete->setParameter('etudiant',$etudiant)->setParameter('element',$element)->setMaxResults(1)->getOneOrNullResult();
	   					
	   					//Si l'affectation existe
	   					if($affectation!=null)
	   					{
	   						//Définir l'enseignement choisi
	   						$enseignement_choisi=$enseignement->getNom();
							
							//Mettre à jour la liste des groupes
							$chaine_groupes=$chaine_groupes.$element->getNom().':'.$affectation->getGroupe()->getId().' ';
	   					}else
	   					{
	   						$probleme=true;
	   						$chaine_groupes=$chaine_groupes.$element->getNom().':? ';
	   					}
	   					
	   				}
	   				
	   				//Si on a trouvé l'enseignement choisi
	   				if($enseignement_choisi!=null)
	   					break;//Pas la peine de vérifier les autres enseignements
	   			
   				}
   				
   				//Si on a pas trouvé l'enseignement choisi
   				if($enseignement_choisi==null)
   				{
   					$choix[]=array('etudiant'=>$etudiant->getNom().' '.$etudiant->getPrenom(),'choix'=>$chaine_choix,'enseignement'=>null,'groupes'=>null,'probleme'=>true);
   				}else//Sinon
   				{
   					$choix[]=array('etudiant'=>$etudiant->getNom().' '.$etudiant->getPrenom(),'choix'=>$chaine_choix,'enseignement'=>$enseignement_choisi,'groupes'=>$chaine_groupes,'probleme'=>$probleme);
   				}
   			}
   		}
   		
   		//Récupération des groupes avec l'effectif de chaque groupe
   		$requete=$em->createQuery('SELECT gr.id as id,count(aff.id) as n FROM AppBundle:Affectation aff JOIN aff.groupe gr GROUP BY gr.id');
   		$lignes=$requete->getResult();
   		
   		$groupes=array();
   		
   		//Conctruction de la liste des objets à retourner
   		foreach($lignes as $ligne)
   		{
   				$groupes[]=array('groupe'=>$ligne['id'],'effectif'=>$ligne['n']);
   		}
        return $this->json(array('obligatoires'=>$obligatoires,'choix' => $choix,'groupes' => $groupes));
    	
      
    }
    
    /**
     * @Route("/solution/sauvegarder_choix", name="sauvegarder_choix")
     */
    public function sauvegarderChoixAction(Request $request)
    {
        try
        {
 		
        	//Récupération du manager doctrine de la base de données
   			$em = $this->getDoctrine()->getManager();
   			
   			//Récupération des choix des étudiants
   			$requete=$em->createQuery('SELECT cho FROM AppBundle:Choix cho');
   			$choix=$requete->getResult();
   			
   			//Création de la requête qui permet de récupérer l'affectation d'un étudiant pour un élément
   			$requete=$em->createQuery('SELECT aff FROM AppBundle:Affectation aff WHERE aff.etudiant=:etudiant AND aff.element=:element');
   	
   			//Récupération de la période actuelle
   			$act=$em->createQuery('SELECT act FROM AppBundle:Sauvegarde\Periode_Actuelle act')->setMaxResults(1)->getOneOrNullResult();
   			$periode=$act->getPeriode();
   			
        	//Parcours de la liste des choix
   			foreach($choix as $choi)
   			{
   				
   				//Récupération des enseignements du choix
   				$enseignements=$choi->getEnseignements();
   				
   				//Parcours de la liste des enseignements
   				foreach($enseignements as $enseignement)
   				{
   				
   					//Récupération de la liste des éléments de l'enseignement
   					$elements=$enseignement->getElements();
   					
   					//Parcours de la liste des éléments
	   				foreach($elements as $element)
	   				{
	   					//Récupération de l'affectation de l'étudiant pour l'élément
	   					$affectation=$requete->setParameter('etudiant',$choi->getEtudiant())->setParameter('element',$element)->setMaxResults(1)->getOneOrNullResult();
	   					
	   					//Si l'affectation existe
	   					if($affectation!=null)
	   					{
	   						//Création d'un choix historique
							$h=new Sauvegarde\Choix_Historique();
							$h->setIdEtudiant($choi->getEtudiant()->getId());
							$h->setIdEnseignement($enseignement->getId());
							$h->setPeriode($periode);
							
							$em->persist($h);
							$em->flush();
							
							//Passer à l'enseignement suivant
							break 2;
	   					}
	   				}
	  	   		}
   			}
        	
        }catch(\Exception $e)
    	{
    		return $this->json(array('etat' => 'erreur','erreur'=>$e->getMessage()));
    	}
    	
        return $this->json(array('etat' => 'sauvegardee'));
    }
    
    /**
     * @Route("/solution/vider_choix", name="vider_choix")
     */
    public function viderChoixAction(Request $request)
    {
        try
        {
 		
        	//Récupération du manager doctrine de la base de données
   			$em = $this->getDoctrine()->getManager();
   			
   			//Récupération de la période actuelle
   			$act=$em->createQuery('SELECT act FROM AppBundle:Sauvegarde\Periode_Actuelle act')->setMaxResults(1)->getOneOrNullResult();
   			$periode=$act->getPeriode();
   			
   			//Suppression des choix historiques
   			$requete=$em->createQuery('DELETE FROM AppBundle:Sauvegarde\Choix_Historique h WHERE h.periode=:periode');
   			$requete->setParameter('periode',$periode);
   			
   			$requete->execute();
           	
        }catch(\Exception $e)
    	{
    		return $this->json(array('etat' => 'erreur','erreur'=>$e->getMessage()));
    	}
    	
        return $this->json(array('etat' => 'videe'));
    }
    
    /**
     * @Route("/solution/imprimer_groupe", name="imprimer_groupe")
     */
    public function imprimerGroupeAction(Request $request)
    {
    	   	
    	//L'id du groupe
    	$id_groupe=$request->query->get('groupe',-1);
        	
       	//Récupération du manager doctrine de la base de données
   		$em = $this->getDoctrine()->getManager();
 		
 		//Récupération des étudiants affectés aux groupes
 		$requete=$em->createQuery('SELECT etu.nom as nom,etu.prenom as prenom FROM AppBundle:Groupe gr JOIN gr.affectations aff JOIN aff.etudiant etu WHERE gr.id=:id_groupe ORDER BY etu.nom,etu.prenom');
 		$requete->setParameter('id_groupe',$id_groupe);
 		$etudiants=$requete->getResult();
 		
 		$contenu='';
 		//Remplissage de la réponse
 		foreach($etudiants as $etudiant)
 		{
 			$contenu=$contenu.$etudiant['nom'].' '.$etudiant['prenom']."\n";
 		}
 	
 		//Création de la réponse
 		$reponse = new Response($contenu);
 		
 		$disposition=$reponse->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT,'Groupe'.$id_groupe.'.txt');

		$reponse->headers->set('Content-Disposition', $disposition);

 		return $reponse;   
    }
}
