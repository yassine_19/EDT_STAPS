<?php

namespace AppBundle\Entity\Sauvegarde;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="Choix_Historique")
 */
class Choix_Historique
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	public $id;
    
    /**
     *@ORM\Column(type="integer")
     */
    private $id_etudiant;
    
    /**
     *@ORM\Column(type="integer")
     */
    private $id_enseignement;
    
    /**
     * @ORM\ManyToOne(targetEntity="Periode", inversedBy="choix_historique")
     * @ORM\JoinColumn(name="periode_id", referencedColumnName="id")
     */
    private $periode;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEtudiant
     *
     * @param integer $idEtudiant
     *
     * @return Choix_Historique
     */
    public function setIdEtudiant($idEtudiant)
    {
        $this->id_etudiant = $idEtudiant;

        return $this;
    }

    /**
     * Get idEtudiant
     *
     * @return integer
     */
    public function getIdEtudiant()
    {
        return $this->id_etudiant;
    }

    /**
     * Set idEnseignement
     *
     * @param integer $idEnseignement
     *
     * @return Choix_Historique
     */
    public function setIdEnseignement($idEnseignement)
    {
        $this->id_enseignement = $idEnseignement;

        return $this;
    }

    /**
     * Get idEnseignement
     *
     * @return integer
     */
    public function getIdEnseignement()
    {
        return $this->id_enseignement;
    }

    /**
     * Set periode
     *
     * @param \AppBundle\Entity\Sauvegarde\Periode $periode
     *
     * @return Choix_Historique
     */
    public function setPeriode(\AppBundle\Entity\Sauvegarde\Periode $periode = null)
    {
        $this->periode = $periode;

        return $this;
    }

    /**
     * Get periode
     *
     * @return \AppBundle\Entity\Sauvegarde\Periode
     */
    public function getPeriode()
    {
        return $this->periode;
    }
}
