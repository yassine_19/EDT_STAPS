<?php

namespace AppBundle\Entity\Sauvegarde;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="Periode")
 */
class Periode
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	public $id;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $annee;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $periode;
    
    /**
     * @ORM\OneToMany(targetEntity="Choix_Historique", mappedBy="periode")
    */
    private $choix_historique;
    
    


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->choix_historique = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annee
     *
     * @param integer $annee
     *
     * @return Periode
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return integer
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set periode
     *
     * @param integer $periode
     *
     * @return Periode
     */
    public function setPeriode($periode)
    {
        $this->periode = $periode;

        return $this;
    }

    /**
     * Get periode
     *
     * @return integer
     */
    public function getPeriode()
    {
        return $this->periode;
    }

    /**
     * Add choixHistorique
     *
     * @param \AppBundle\Entity\Sauvegarde\Choix_Historique $choixHistorique
     *
     * @return Periode
     */
    public function addChoixHistorique(\AppBundle\Entity\Sauvegarde\Choix_Historique $choixHistorique)
    {
        $this->choix_historique[] = $choixHistorique;

        return $this;
    }

    /**
     * Remove choixHistorique
     *
     * @param \AppBundle\Entity\Sauvegarde\Choix_Historique $choixHistorique
     */
    public function removeChoixHistorique(\AppBundle\Entity\Sauvegarde\Choix_Historique $choixHistorique)
    {
        $this->choix_historique->removeElement($choixHistorique);
    }

    /**
     * Get choixHistorique
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChoixHistorique()
    {
        return $this->choix_historique;
    }
}
