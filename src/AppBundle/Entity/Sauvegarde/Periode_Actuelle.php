<?php

namespace AppBundle\Entity\Sauvegarde;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="Periode_Actuelle")
 */
class Periode_Actuelle
{
	/**
	 * @ORM\Id
     * @ORM\OneToOne(targetEntity="Periode")
     * @ORM\JoinColumn(name="periode_id", referencedColumnName="id")
     */
	public $periode;
    

    /**
     * Set periode
     *
     * @param \AppBundle\Entity\Sauvegarde\Periode $periode
     *
     * @return Periode_Actuelle
     */
    public function setPeriode(\AppBundle\Entity\Sauvegarde\Periode $periode = null)
    {
        $this->periode = $periode;

        return $this;
    }

    /**
     * Get periode
     *
     * @return \AppBundle\Entity\Sauvegarde\Periode
     */
    public function getPeriode()
    {
        return $this->periode;
    }
}
