<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;



/**
 * @ORM\Entity
 * @ORM\Table(name="Enseignement")
 */
class Enseignement
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
	private $id;
	
	/**
     * @ORM\Column(type="string", length=100)
     */
	private $nom;
	
	/**
     * @ORM\Column(type="integer")
     */
    private $priorite;
    
    /**
     * @ORM\OneToMany(targetEntity="Element", mappedBy="enseignement")
     */
    private $elements;
    
    /**
     * @ORM\ManyToMany(targetEntity="Etudiant", mappedBy="enseignements")
     */
    private $etudiants;
    
    /**
     * @ORM\ManyToMany(targetEntity="Choix", mappedBy="enseignements")
     */
    private $choix;
    
    
	public function __construct()
    {
        $this->elements = new ArrayCollection();
        $this->etudiants= new ArrayCollection();
        $this->choix=new ArrayCollection();
    }
    

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Enseignement
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Enseignement
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set priorite
     *
     * @param integer $priorite
     *
     * @return Enseignement
     */
    public function setPriorite($priorite)
    {
        $this->priorite = $priorite;

        return $this;
    }

    /**
     * Get priorite
     *
     * @return integer
     */
    public function getPriorite()
    {
        return $this->priorite;
    }

    /**
     * Add element
     *
     * @param \AppBundle\Entity\Element $element
     *
     * @return Enseignement
     */
    public function addElement(\AppBundle\Entity\Element $element)
    {
        $this->elements[] = $element;

        return $this;
    }

    /**
     * Remove element
     *
     * @param \AppBundle\Entity\Element $element
     */
    public function removeElement(\AppBundle\Entity\Element $element)
    {
        $this->elements->removeElement($element);
    }

    /**
     * Get elements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * Add etudiant
     *
     * @param \AppBundle\Entity\Etudiant $etudiant
     *
     * @return Enseignement
     */
    public function addEtudiant(\AppBundle\Entity\Etudiant $etudiant)
    {
        $this->etudiants[] = $etudiant;

        return $this;
    }

    /**
     * Remove etudiant
     *
     * @param \AppBundle\Entity\Etudiant $etudiant
     */
    public function removeEtudiant(\AppBundle\Entity\Etudiant $etudiant)
    {
        $this->etudiants->removeElement($etudiant);
    }

    /**
     * Get etudiants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtudiants()
    {
        return $this->etudiants;
    }

    /**
     * Add choix
     *
     * @param \AppBundle\Entity\Choix $choix
     *
     * @return Enseignement
     */
    public function addChoix(\AppBundle\Entity\Choix $choix)
    {
        $this->choix[] = $choix;

        return $this;
    }

    /**
     * Remove choix
     *
     * @param \AppBundle\Entity\Choix $choix
     */
    public function removeChoix(\AppBundle\Entity\Choix $choix)
    {
        $this->choix->removeElement($choix);
    }

    /**
     * Get choix
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChoix()
    {
        return $this->choix;
    }
}

?>
