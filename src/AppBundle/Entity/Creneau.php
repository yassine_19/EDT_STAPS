<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="Creneau")
 */
class Creneau
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	public $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Groupe", inversedBy="creneaux")
     * @ORM\JoinColumn(name="groupe_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $groupe;
    
    /**
     * @ORM\ManyToOne(targetEntity="Fixe", inversedBy="creneaux")
     * @ORM\JoinColumn(name="fixe_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $fixe;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $date;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $duree;
    
    
    public function __construct()
    {
    }
    
    public function intersection($creneau)
    {
    	if($this->date->getTimeStamp()==$creneau->date->getTimeStamp())
    		return true;
    		
    	if($this->date->getTimeStamp()<$creneau->date->getTimeStamp())
    	{
    		if(($this->date->getTimeStamp()+$this->duree*60)<=$creneau->date->getTimeStamp())
    			return false;
    		else
    			return true;	
    	}else
    	{
    		if(($creneau->date->getTimeStamp()+$creneau->duree*60)<=$this->date->getTimeStamp())
    			return false;
    		else
    			return true;
    	}
    }
    
    public function __toString()
    {
        return (string)$this->id;
    }
    

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Creneau
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Creneau
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set duree
     *
     * @param integer $duree
     *
     * @return Creneau
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return integer
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set groupe
     *
     * @param \AppBundle\Entity\Groupe $groupe
     *
     * @return Creneau
     */
    public function setGroupe(\AppBundle\Entity\Groupe $groupe = null)
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * Get groupe
     *
     * @return \AppBundle\Entity\Groupe
     */
    public function getGroupe()
    {
        return $this->groupe;
    }

    /**
     * Set fixe
     *
     * @param \AppBundle\Entity\Fixe $fixe
     *
     * @return Creneau
     */
    public function setFixe(\AppBundle\Entity\Fixe $fixe = null)
    {
        $this->fixe = $fixe;

        return $this;
    }

    /**
     * Get fixe
     *
     * @return \AppBundle\Entity\Fixe
     */
    public function getFixe()
    {
        return $this->fixe;
    }
}
