<?php


namespace AppBundle\Entity\Maquette;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;



/**
 * @ORM\Entity
 * @ORM\Table(name="Maq_UE")
 */
class UE
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
	private $id;
	
	/**
     * @ORM\Column(type="string", length=100)
     */
	private $intitule;
	
	/**
     * @ORM\Column(type="boolean")
     */
    private $libre;
	
	/**
     * @ORM\OneToMany(targetEntity="Choix", mappedBy="ue")
     */
    private $choix;
    
    /**
     * @ORM\OneToMany(targetEntity="EC", mappedBy="ue")
     */
    private $ecs;
    
	public function __construct()
    {
        $this->choix = new ArrayCollection();
        $this->ecs= new ArrayCollection();
    }
    

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return UE
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intitule
     *
     * @param string $intitule
     *
     * @return UE
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule
     *
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * Set libre
     *
     * @param boolean $libre
     *
     * @return UE
     */
    public function setLibre($libre)
    {
        $this->libre = $libre;

        return $this;
    }

    /**
     * Get libre
     *
     * @return boolean
     */
    public function getLibre()
    {
        return $this->libre;
    }

    /**
     * Add choix
     *
     * @param \AppBundle\Entity\Maquette\Choix $choix
     *
     * @return UE
     */
    public function addChoix(\AppBundle\Entity\Maquette\Choix $choix)
    {
        $this->choix[] = $choix;

        return $this;
    }

    /**
     * Remove choix
     *
     * @param \AppBundle\Entity\Maquette\Choix $choix
     */
    public function removeChoix(\AppBundle\Entity\Maquette\Choix $choix)
    {
        $this->choix->removeElement($choix);
    }

    /**
     * Get choix
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChoix()
    {
        return $this->choix;
    }

    /**
     * Add ec
     *
     * @param \AppBundle\Entity\Maquette\EC $ec
     *
     * @return UE
     */
    public function addEc(\AppBundle\Entity\Maquette\EC $ec)
    {
        $this->ecs[] = $ec;

        return $this;
    }

    /**
     * Remove ec
     *
     * @param \AppBundle\Entity\Maquette\EC $ec
     */
    public function removeEc(\AppBundle\Entity\Maquette\EC $ec)
    {
        $this->ecs->removeElement($ec);
    }

    /**
     * Get ecs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEcs()
    {
        return $this->ecs;
    }
}
