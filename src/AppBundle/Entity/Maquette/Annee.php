<?php


namespace AppBundle\Entity\Maquette;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;



/**
 * @ORM\Entity
 * @ORM\Table(name="Maq_Annee")
 */
class Annee
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;
	
	
	/**
     * @ORM\Column(type="integer")
     */
    private $numero;
   
    /**
     * @ORM\ManyToOne(targetEntity="Parcours",inversedBy="annees")
     * @ORM\JoinColumn(name="parcours_id", referencedColumnName="id")
     */
    private $parcours;
    
    /**
     * @ORM\ManyToMany(targetEntity="UE")
     * @ORM\JoinTable(name="Maq_Annee_UE")
     */
    private $ues;
    
    
	public function __construct()
    {
    	$this->ues=new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Annee
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set parcours
     *
     * @param \AppBundle\Entity\Maquette\Parcours $parcours
     *
     * @return Annee
     */
    public function setParcours(\AppBundle\Entity\Maquette\Parcours $parcours = null)
    {
        $this->parcours = $parcours;

        return $this;
    }

    /**
     * Get parcours
     *
     * @return \AppBundle\Entity\Maquette\Parcours
     */
    public function getParcours()
    {
        return $this->parcours;
    }

    /**
     * Add ue
     *
     * @param \AppBundle\Entity\Maquette\UE $ue
     *
     * @return Annee
     */
    public function addUe(\AppBundle\Entity\Maquette\UE $ue)
    {
        $this->ues[] = $ue;

        return $this;
    }

    /**
     * Remove ue
     *
     * @param \AppBundle\Entity\Maquette\UE $ue
     */
    public function removeUe(\AppBundle\Entity\Maquette\UE $ue)
    {
        $this->ues->removeElement($ue);
    }

    /**
     * Get ues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUes()
    {
        return $this->ues;
    }
}
