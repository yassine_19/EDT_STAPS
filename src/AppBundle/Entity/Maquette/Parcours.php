<?php


namespace AppBundle\Entity\Maquette;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;



/**
 * @ORM\Entity
 * @ORM\Table(name="Maq_Parcours")
 */
class Parcours
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
	private $id;
	
    /**
     * @ORM\OneToMany(targetEntity="Annee", mappedBy="parcours")
     */
    private $annees;
    
	public function __construct()
    {
        $this->annees = new ArrayCollection();
    }
    
	/**
     * Set id
     *
     * @param integer $id
     *
     * @return EC
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add annee
     *
     * @param \AppBundle\Entity\Maquette\Annee $annee
     *
     * @return Parcours
     */
    public function addAnnee(\AppBundle\Entity\Maquette\Annee $annee)
    {
        $this->annees[] = $annee;

        return $this;
    }

    /**
     * Remove annee
     *
     * @param \AppBundle\Entity\Maquette\Annee $annee
     */
    public function removeAnnee(\AppBundle\Entity\Maquette\Annee $annee)
    {
        $this->annees->removeElement($annee);
    }

    /**
     * Get annees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnnees()
    {
        return $this->annees;
    }
}
