<?php


namespace AppBundle\Entity\Maquette;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;



/**
 * @ORM\Entity
 * @ORM\Table(name="Maq_EC")
 */
class EC
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
	private $id;
	
	/**
     * @ORM\Column(type="string", length=100)
     */
	private $intitule;
	
	/**
     * @ORM\Column(type="boolean")
     */
    private $cours;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $td;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $tp;
    
  	/**
     * @ORM\ManyToOne(targetEntity="UE",inversedBy="ecs")
     * @ORM\JoinColumn(name="ue_id", referencedColumnName="id")
     */
    private $ue;
    
    /**
     * @ORM\ManyToMany(targetEntity="Parcours")
     * @ORM\JoinTable(name="Maq_EC_Parcours")
     */
    private $parcours;
    
	public function __construct()
    {
    	$this->parcours=new ArrayCollection();
    }
    
    public function addParcours(\AppBundle\Entity\Maquette\Parcours $parcours)
    {
    	$this->parcours[] = $parcours;

        return $this;
    }
    

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return EC
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intitule
     *
     * @param string $intitule
     *
     * @return EC
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule
     *
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * Set cours
     *
     * @param boolean $cours
     *
     * @return EC
     */
    public function setCours($cours)
    {
        $this->cours = $cours;

        return $this;
    }

    /**
     * Get cours
     *
     * @return boolean
     */
    public function getCours()
    {
        return $this->cours;
    }

    /**
     * Set td
     *
     * @param boolean $td
     *
     * @return EC
     */
    public function setTd($td)
    {
        $this->td = $td;

        return $this;
    }

    /**
     * Get td
     *
     * @return boolean
     */
    public function getTd()
    {
        return $this->td;
    }

    /**
     * Set tp
     *
     * @param boolean $tp
     *
     * @return EC
     */
    public function setTp($tp)
    {
        $this->tp = $tp;

        return $this;
    }

    /**
     * Get tp
     *
     * @return boolean
     */
    public function getTp()
    {
        return $this->tp;
    }

    /**
     * Set ue
     *
     * @param \AppBundle\Entity\Maquette\UE $ue
     *
     * @return EC
     */
    public function setUe(\AppBundle\Entity\Maquette\UE $ue = null)
    {
        $this->ue = $ue;

        return $this;
    }

    /**
     * Get ue
     *
     * @return \AppBundle\Entity\Maquette\UE
     */
    public function getUe()
    {
        return $this->ue;
    }
}
