<?php


namespace AppBundle\Entity\Maquette;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;



/**
 * @ORM\Entity
 * @ORM\Table(name="Maq_Choix")
 */
class Choix
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;
	
	/**
     * @ORM\Column(type="string", length=100)
     */
	private $intitule;
	
	
    /**
     * @ORM\ManyToOne(targetEntity="UE",inversedBy="choix")
     * @ORM\JoinColumn(name="ue_id", referencedColumnName="id")
     */
    private $ue;
    
    /**
     * @ORM\ManyToMany(targetEntity="EC")
     * @ORM\JoinTable(name="Maq_Choix_EC")
     */
    private $ecs;
    
	public function __construct()
    {
        $this->ecs = new ArrayCollection();
    }
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intitule
     *
     * @param string $intitule
     *
     * @return Choix
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule
     *
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * Set ue
     *
     * @param \AppBundle\Entity\Maquette\UE $ue
     *
     * @return Choix
     */
    public function setUe(\AppBundle\Entity\Maquette\UE $ue = null)
    {
        $this->ue = $ue;

        return $this;
    }

    /**
     * Get ue
     *
     * @return \AppBundle\Entity\Maquette\UE
     */
    public function getUe()
    {
        return $this->ue;
    }

    /**
     * Add ec
     *
     * @param \AppBundle\Entity\Maquette\EC $ec
     *
     * @return Choix
     */
    public function addEc(\AppBundle\Entity\Maquette\EC $ec)
    {
        $this->ecs[] = $ec;

        return $this;
    }

    /**
     * Remove ec
     *
     * @param \AppBundle\Entity\Maquette\EC $ec
     */
    public function removeEc(\AppBundle\Entity\Maquette\EC $ec)
    {
        $this->ecs->removeElement($ec);
    }

    /**
     * Get ecs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEcs()
    {
        return $this->ecs;
    }
}
