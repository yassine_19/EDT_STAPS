<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="Affectation")
 */
class Affectation
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	public $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Etudiant", inversedBy="affectations")
     * @ORM\JoinColumn(name="etudiant_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $etudiant;
    
    /**
     * @ORM\ManyToOne(targetEntity="Element", inversedBy="affectations")
     * @ORM\JoinColumn(name="element_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $element;
    
    /**
     * @ORM\ManyToOne(targetEntity="Groupe", inversedBy="affectations")
     * @ORM\JoinColumn(name="groupe_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $groupe;

    
    public function __construct()
    {
    }
    
    

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Affectation
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set etudiant
     *
     * @param \AppBundle\Entity\Etudiant $etudiant
     *
     * @return Affectation
     */
    public function setEtudiant(\AppBundle\Entity\Etudiant $etudiant = null)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant
     *
     * @return \AppBundle\Entity\Etudiant
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set element
     *
     * @param \AppBundle\Entity\Element $element
     *
     * @return Affectation
     */
    public function setElement(\AppBundle\Entity\Element $element = null)
    {
        $this->element = $element;

        return $this;
    }

    /**
     * Get element
     *
     * @return \AppBundle\Entity\Element
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * Set groupe
     *
     * @param \AppBundle\Entity\Groupe $groupe
     *
     * @return Affectation
     */
    public function setGroupe(\AppBundle\Entity\Groupe $groupe = null)
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * Get groupe
     *
     * @return \AppBundle\Entity\Groupe
     */
    public function getGroupe()
    {
        return $this->groupe;
    }
}
