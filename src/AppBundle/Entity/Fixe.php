<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="Fixe")
 */
class Fixe
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	public $id;
	
	/**
     * @ORM\OneToMany(targetEntity="Creneau", mappedBy="fixe")
     */
    private $creneaux;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
	private $lieu;
	
	/**
     * @ORM\Column(type="string", length=100)
     */
	private $enseignant;
    
    /**
     * @ORM\ManyToOne(targetEntity="Etudiant", inversedBy="fixes")
     * @ORM\JoinColumn(name="etudiant_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $etudiant;
    
    /**
     * @ORM\ManyToOne(targetEntity="Element")
     * @ORM\JoinColumn(name="element_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $element;
    
    public function __construct()
    {
        $this->creneaux=new ArrayCollection();
    }
    
    

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Fixe
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     *
     * @return Fixe
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set enseignant
     *
     * @param string $enseignant
     *
     * @return Fixe
     */
    public function setEnseignant($enseignant)
    {
        $this->enseignant = $enseignant;

        return $this;
    }

    /**
     * Get enseignant
     *
     * @return string
     */
    public function getEnseignant()
    {
        return $this->enseignant;
    }

    /**
     * Add creneaux
     *
     * @param \AppBundle\Entity\Creneau $creneaux
     *
     * @return Fixe
     */
    public function addCreneaux(\AppBundle\Entity\Creneau $creneaux)
    {
        $this->creneaux[] = $creneaux;

        return $this;
    }

    /**
     * Remove creneaux
     *
     * @param \AppBundle\Entity\Creneau $creneaux
     */
    public function removeCreneaux(\AppBundle\Entity\Creneau $creneaux)
    {
        $this->creneaux->removeElement($creneaux);
    }

    /**
     * Get creneaux
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCreneaux()
    {
        return $this->creneaux;
    }

    /**
     * Set etudiant
     *
     * @param \AppBundle\Entity\Etudiant $etudiant
     *
     * @return Fixe
     */
    public function setEtudiant(\AppBundle\Entity\Etudiant $etudiant = null)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant
     *
     * @return \AppBundle\Entity\Etudiant
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Set element
     *
     * @param \AppBundle\Entity\Element $element
     *
     * @return Fixe
     */
    public function setElement(\AppBundle\Entity\Element $element = null)
    {
        $this->element = $element;

        return $this;
    }

    /**
     * Get element
     *
     * @return \AppBundle\Entity\Element
     */
    public function getElement()
    {
        return $this->element;
    }
}
