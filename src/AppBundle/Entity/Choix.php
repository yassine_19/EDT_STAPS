<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="Choix")
 */
class Choix
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	public $id;
    
    private $affecte;
    
    /**
     * @ORM\ManyToOne(targetEntity="Etudiant", inversedBy="choix")
     * @ORM\JoinColumn(name="etudiant_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $etudiant;
    
    /**
     * @ORM\ManyToMany(targetEntity="Enseignement", inversedBy="choix")
     * @ORM\JoinTable(name="Choix_Enseignement")
     */
    private $enseignements;
    
    public function __construct()
    {
        $this->enseignements=new ArrayCollection();
    }
    
   	/**
     * @ORM\PostLoad
     */
    public function initialiserAffecte()
    {
    	 $this->affecte=false;
    }

    public function setEnseignements($enseignements)
    {
        
        $this->enseignements=new ArrayCollection($enseignements);
        return $this;
    }
    
    public function isAffecte()
    {
    	return $this->affecte;
    }
    
    public function rendreAffecte()
    {
		$this->affecte=true;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Choix
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set etudiant
     *
     * @param \AppBundle\Entity\Etudiant $etudiant
     *
     * @return Choix
     */
    public function setEtudiant(\AppBundle\Entity\Etudiant $etudiant = null)
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    /**
     * Get etudiant
     *
     * @return \AppBundle\Entity\Etudiant
     */
    public function getEtudiant()
    {
        return $this->etudiant;
    }

    /**
     * Add enseignement
     *
     * @param \AppBundle\Entity\Enseignement $enseignement
     *
     * @return Choix
     */
    public function addEnseignement(\AppBundle\Entity\Enseignement $enseignement)
    {
        $this->enseignements[] = $enseignement;

        return $this;
    }

    /**
     * Remove enseignement
     *
     * @param \AppBundle\Entity\Enseignement $enseignement
     */
    public function removeEnseignement(\AppBundle\Entity\Enseignement $enseignement)
    {
        $this->enseignements->removeElement($enseignement);
    }

    /**
     * Get enseignements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnseignements()
    {
        return $this->enseignements;
    }
}
