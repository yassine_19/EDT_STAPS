<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="Etudiant")
 * @ORM\HasLifecycleCallbacks
 */
class Etudiant
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
	public $id;
	
	
	/**
     * @ORM\Column(type="string", length=100)
     */
	private $nom;
	
	/**
     * @ORM\Column(type="string", length=100)
     */
	private $prenom;
	
	private $creneaux_remplis;
	
	
	/**
     * @ORM\OneToMany(targetEntity="Affectation", mappedBy="etudiant")
     */
    private $affectations;
    
    /**
     * @ORM\OneToMany(targetEntity="Fixe", mappedBy="etudiant")
     */
    private $fixes;
    
    
    /**
     * @ORM\ManyToMany(targetEntity="Enseignement", inversedBy="etudiants")
     * @ORM\JoinTable(name="Etudiant_Enseignement")
     */
    private $enseignements;
    
    /**
     * @ORM\OneToMany(targetEntity="Choix", mappedBy="etudiant")
     */
    private $choix;
    
    
    public function __construct()
    {
        $this->affectations=new ArrayCollection();
        $this->fixes=new ArrayCollection();
        $this->enseignements=new ArrayCollection();
        $this->choix=new ArrayCollection();        
    }
    
    /**
     * @ORM\PostLoad
     */
    public function initialiserCreneauxRemplis()
    {
    	$this->creneaux_remplis=array();
    	foreach($this->fixes as $fixe)
    	{
    		$this->creneaux_remplis=array_merge($this->creneaux_remplis,$fixe->getCreneaux()->toArray());
    	}
    	
    }


    public function setEnseignements($enseignements)
    {
        
        $this->enseignements=new ArrayCollection($enseignements);
        return $this;
    }
    
    public function verifierDisponibilite($groupe)
    {
    	foreach($groupe->getCreneaux() as $creneau_groupe)
    	{
    		foreach($this->creneaux_remplis as $creneau)
    		{
    			if($creneau->intersection($creneau_groupe))
    			{
    				return false;
    			}
    		}
    	}
    	return true;
    }
	
	public function ajouterCreneaux($groupe)
	{
		$this->creneaux_remplis=array_merge($this->creneaux_remplis,$groupe->getCreneaux()->toArray());
	}
	
	public function enleverCreneaux($groupe)
	{
		$this->creneaux_remplis=array_diff($this->creneaux_remplis,$groupe->getCreneaux()->toArray());
	}
    
    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Etudiant
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Etudiant
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Etudiant
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Add affectation
     *
     * @param \AppBundle\Entity\Affectation $affectation
     *
     * @return Etudiant
     */
    public function addAffectation(\AppBundle\Entity\Affectation $affectation)
    {
        $this->affectations[] = $affectation;

        return $this;
    }

    /**
     * Remove affectation
     *
     * @param \AppBundle\Entity\Affectation $affectation
     */
    public function removeAffectation(\AppBundle\Entity\Affectation $affectation)
    {
        $this->affectations->removeElement($affectation);
    }

    /**
     * Get affectations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAffectations()
    {
        return $this->affectations;
    }

    /**
     * Add fix
     *
     * @param \AppBundle\Entity\Fixe $fix
     *
     * @return Etudiant
     */
    public function addFix(\AppBundle\Entity\Fixe $fix)
    {
        $this->fixes[] = $fix;

        return $this;
    }

    /**
     * Remove fix
     *
     * @param \AppBundle\Entity\Fixe $fix
     */
    public function removeFix(\AppBundle\Entity\Fixe $fix)
    {
        $this->fixes->removeElement($fix);
    }

    /**
     * Get fixes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFixes()
    {
        return $this->fixes;
    }

    /**
     * Add enseignement
     *
     * @param \AppBundle\Entity\Enseignement $enseignement
     *
     * @return Etudiant
     */
    public function addEnseignement(\AppBundle\Entity\Enseignement $enseignement)
    {
        $this->enseignements[] = $enseignement;

        return $this;
    }

    /**
     * Remove enseignement
     *
     * @param \AppBundle\Entity\Enseignement $enseignement
     */
    public function removeEnseignement(\AppBundle\Entity\Enseignement $enseignement)
    {
        $this->enseignements->removeElement($enseignement);
    }

    /**
     * Get enseignements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnseignements()
    {
        return $this->enseignements;
    }

    /**
     * Add choix
     *
     * @param \AppBundle\Entity\Choix $choix
     *
     * @return Etudiant
     */
    public function addChoix(\AppBundle\Entity\Choix $choix)
    {
        $this->choix[] = $choix;

        return $this;
    }

    /**
     * Remove choix
     *
     * @param \AppBundle\Entity\Choix $choix
     */
    public function removeChoix(\AppBundle\Entity\Choix $choix)
    {
        $this->choix->removeElement($choix);
    }

    /**
     * Get choix
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChoix()
    {
        return $this->choix;
    }
}
