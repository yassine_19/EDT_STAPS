<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="Element")
 * @ORM\HasLifecycleCallbacks
 */
class Element
{
	
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	public $id;
	
	
	/**
     * @ORM\Column(type="string", length=100)
     */
	private $nom;
	
	private $groupes_disponibles;
    
    
	/**
     * @ORM\ManyToOne(targetEntity="Enseignement", inversedBy="elements")
     * @ORM\JoinColumn(name="enseignement_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $enseignement;
    
    /**
     * @ORM\OneToMany(targetEntity="Groupe", mappedBy="element")
     */
    private $groupes;
    
    /**
     * @ORM\OneToMany(targetEntity="Affectation", mappedBy="element")
     */
    private $affectations;
    
    public function __construct()
    {
        $this->groupes=new ArrayCollection();
        $this->affectations=new ArrayCollection();
    }
    
    /**
     * @ORM\PostLoad
     */
    public function initialiserGroupesDisponibles()
    {
    	$this->groupes_disponibles = array();
    	foreach($this->groupes as $groupe)
    	{
    		$this->groupes_disponibles[$groupe->getId()]=$groupe;
    	}
    }
    
    public function getGroupesDisponibles()
    {
    	return $this->groupes_disponibles;
    }
    
    public function mettreAJourGroupesDisponibles($groupe)
    {
    	if($groupe->isPlein())
    		unset($this->groupes_disponibles[$groupe->getId()]);
    }
    

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Element
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Element
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set enseignement
     *
     * @param \AppBundle\Entity\Enseignement $enseignement
     *
     * @return Element
     */
    public function setEnseignement(\AppBundle\Entity\Enseignement $enseignement = null)
    {
        $this->enseignement = $enseignement;

        return $this;
    }

    /**
     * Get enseignement
     *
     * @return \AppBundle\Entity\Enseignement
     */
    public function getEnseignement()
    {
        return $this->enseignement;
    }

    /**
     * Add groupe
     *
     * @param \AppBundle\Entity\Groupe $groupe
     *
     * @return Element
     */
    public function addGroupe(\AppBundle\Entity\Groupe $groupe)
    {
        $this->groupes[] = $groupe;
        
        $this->groupes_remplis[$groupe->getId()]=false;

        return $this;
    }

    /**
     * Remove groupe
     *
     * @param \AppBundle\Entity\Groupe $groupe
     */
    public function removeGroupe(\AppBundle\Entity\Groupe $groupe)
    {
        $this->groupes->removeElement($groupe);
    }

    /**
     * Get groupes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupes()
    {
        return $this->groupes;
    }

    /**
     * Add affectation
     *
     * @param \AppBundle\Entity\Affectation $affectation
     *
     * @return Element
     */
    public function addAffectation(\AppBundle\Entity\Affectation $affectation)
    {
        $this->affectations[] = $affectation;

        return $this;
    }

    /**
     * Remove affectation
     *
     * @param \AppBundle\Entity\Affectation $affectation
     */
    public function removeAffectation(\AppBundle\Entity\Affectation $affectation)
    {
        $this->affectations->removeElement($affectation);
    }

    /**
     * Get affectations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAffectations()
    {
        return $this->affectations;
    }
}
