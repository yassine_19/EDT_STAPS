<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="Groupe")
 * @ORM\HasLifecycleCallbacks
 */
class Groupe
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
	public $id;
	
	/**
     * @ORM\Column(type="string", length=100)
     */
	private $lieu;
	
	/**
     * @ORM\Column(type="string", length=100)
     */
	private $enseignant;
	
	/**
     * @ORM\Column(type="integer")
     */
    private $effectif_total;
    
    /**
     * @ORM\OneToMany(targetEntity="Creneau", mappedBy="groupe")
     */
    private $creneaux;
    
    private $effectif_actuel;
    
    /**
     * @ORM\ManyToOne(targetEntity="Element", inversedBy="groupes")
     * @ORM\JoinColumn(name="element_id", referencedColumnName="id",onDelete="CASCADE")
     */
    private $element;
    
    /**
     * @ORM\OneToMany(targetEntity="Affectation", mappedBy="groupe")
     */
    private $affectations;
    
    public function __construct()
    {
        $this->creneaux=new ArrayCollection();
        $this->affectations=new ArrayCollection();
        
    }
    
    /**
     * @ORM\PostLoad
     */
    public function initialiserEffectifActuel()
    {
   		$this->effectif_actuel = 0;
    }
	
	public function incrementerEffectif()
	{
		$this->effectif_actuel++;
	}
	
	public function isPlein()
	{
		return $this->effectif_actuel==$this->effectif_total;
	}

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Groupe
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     *
     * @return Groupe
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set enseignant
     *
     * @param string $enseignant
     *
     * @return Groupe
     */
    public function setEnseignant($enseignant)
    {
        $this->enseignant = $enseignant;

        return $this;
    }

    /**
     * Get enseignant
     *
     * @return string
     */
    public function getEnseignant()
    {
        return $this->enseignant;
    }

    /**
     * Set effectifTotal
     *
     * @param integer $effectifTotal
     *
     * @return Groupe
     */
    public function setEffectifTotal($effectifTotal)
    {
        $this->effectif_total = $effectifTotal;

        return $this;
    }

    /**
     * Get effectifTotal
     *
     * @return integer
     */
    public function getEffectifTotal()
    {
        return $this->effectif_total;
    }

    /**
     * Add creneaux
     *
     * @param \AppBundle\Entity\Creneau $creneaux
     *
     * @return Groupe
     */
    public function addCreneaux(\AppBundle\Entity\Creneau $creneaux)
    {
        $this->creneaux[] = $creneaux;

        return $this;
    }

    /**
     * Remove creneaux
     *
     * @param \AppBundle\Entity\Creneau $creneaux
     */
    public function removeCreneaux(\AppBundle\Entity\Creneau $creneaux)
    {
        $this->creneaux->removeElement($creneaux);
    }

    /**
     * Get creneaux
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCreneaux()
    {
        return $this->creneaux;
    }

    /**
     * Set element
     *
     * @param \AppBundle\Entity\Element $element
     *
     * @return Groupe
     */
    public function setElement(\AppBundle\Entity\Element $element = null)
    {
        $this->element = $element;

        return $this;
    }

    /**
     * Get element
     *
     * @return \AppBundle\Entity\Element
     */
    public function getElement()
    {
        return $this->element;
    }

    /**
     * Add affectation
     *
     * @param \AppBundle\Entity\Affectation $affectation
     *
     * @return Groupe
     */
    public function addAffectation(\AppBundle\Entity\Affectation $affectation)
    {
        $this->affectations[] = $affectation;

        return $this;
    }

    /**
     * Remove affectation
     *
     * @param \AppBundle\Entity\Affectation $affectation
     */
    public function removeAffectation(\AppBundle\Entity\Affectation $affectation)
    {
        $this->affectations->removeElement($affectation);
    }

    /**
     * Get affectations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAffectations()
    {
        return $this->affectations;
    }
}
